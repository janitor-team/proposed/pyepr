Source: pyepr
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Antonio Valentino <antonio.valentino@tiscali.it>
Section: python
Priority: optional
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-python
Build-Depends: debhelper-compat (= 12),
               dh-python,
               cython3,
               libepr-api-dev,
               pybuild-plugin-pyproject,
               python3-all-dev,
               python3-doc,
               python3-ipython,
               python3-numpy,
               python3-packaging,
               python3-setuptools,
               python3-sphinx,
               python3-sphinx-rtd-theme,
               texlive-latex-extra
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian-gis-team/pyepr
Vcs-Git: https://salsa.debian.org/debian-gis-team/pyepr.git
Homepage: https://avalentino.github.io/pyepr

Package: python3-epr
Architecture: any
Multi-Arch: same
Depends: ${python3:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Suggests: python-epr-doc
Description: Python ENVISAT Product Reader API (Python 3)
 PyEPR provides Python bindings for the ENVISAT Product Reader C API
 (EPR API) for reading satellite data from ENVISAT ESA (European Space
 Agency) mission.
 .
 PyEPR, as well as the EPR API for C, supports ENVISAT MERIS, AATSR
 Level 1B and Level 2 and also ASAR data products. It provides access
 to the data either on a geophysical (decoded, ready-to-use pixel
 samples) or on a raw data layer. The raw data access makes it possible
 to read any data field contained in a product file.
 .
 This package contains pyepr for Python 3.

Package: python-epr-doc
Architecture: all
Section: doc
Multi-Arch: foreign
Depends: ${sphinxdoc:Depends},
         ${misc:Depends}
Suggests: www-browser
Description: Python ENVISAT Product Reader API (common documentation)
 PyEPR provides Python bindings for the ENVISAT Product Reader C API
 (EPR API) for reading satellite data from ENVISAT ESA (European Space
 Agency) mission.
 .
 PyEPR, as well as the EPR API for C, supports ENVISAT MERIS, AATSR
 Level 1B and Level 2 and also ASAR data products. It provides access
 to the data either on a geophysical (decoded, ready-to-use pixel
 samples) or on a raw data layer. The raw data access makes it possible
 to read any data field contained in a product file.
 .
 This is the common documentation package.
